## Spring Cloud Data Flow

Microservice based Streaming and Batch data processing for Cloud Foundry and Kubernetes.

- Spring Cloud Data Flow provides tools to create complex topologies for streaming and batch data pipelines. 
- The data pipelines consist of Spring Boot apps, built using the Spring Cloud Stream or Spring Cloud Task microservice frameworks.
- It supports a range of data processing use cases, from ETL to import/export, event streaming, and predictive analytics.
- Installation env:
	- local machine (Docker Compose)
	- Cloud Foundry
	- Kubernetes

### [Local Installation](https://dataflow.spring.io/docs/installation/local/docker/)
Spring Cloud Data Flow provides a Docker Compose file to let you quickly bring up Spring Cloud Data Flow, Skipper, and MySQL, Apache Kafka

- To download the Spring Cloud Data Flow Server Docker Compose file
```bash
wget https://raw.githubusercontent.com/spring-cloud/spring-cloud-dataflow/master/spring-cloud-dataflow-server/docker-compose.yml
```

- The Docker Compose file starts instances of the following products:
	- Spring Cloud Data Flow Server
	- Spring Cloud Skipper Server
	- MySQL
	- Apache Kafka
	- Monitoring with Prometheus and Grafana(https://dataflow.spring.io/docs/installation/local/docker-customize/)

- Starting Docker Compose
```bash
DATAFLOW_VERSION=2.4.2.RELEASE SKIPPER_VERSION=2.3.2.RELEASE docker-compose up
```

## Monitoring

By default, the Data Flow docker-compose does not enable the monitoring functionality for Stream and Task applications. To enable that, follow the [Monitoring with Prometheus and Grafana or Monitoring with InfluxDB and Grafana](https://dataflow.spring.io/docs/installation/local/docker-customize/#monitoring-with-influxdb-and-grafana) sections for how to configure Prometheus or InfluxDB based monitoring set up for Spring Cloud Data Flow.
To learn more about the monitoring experience in Spring Cloud Data Flow with Prometheus and InfluxDB, see the [Stream Monitoring](https://dataflow.spring.io/docs/feature-guides/streams/monitoring/#local) feature guide.

- To download the Prometheus and Grafana Docker Compose file
```bash
wget https://raw.githubusercontent.com/spring-cloud/spring-cloud-dataflow/master/spring-cloud-dataflow-server/docker-compose-prometheus.yml
```

- to enable the Stream and Task monitoring with Prometheus and Grafana
```bash
DATAFLOW_VERSION=2.4.2.RELEASE SKIPPER_VERSION=2.3.2.RELEASE docker-compose -f ./docker-compose.yml -f ./docker-compose-prometheus.yml up
```

## Spring Cloud Task
Spring Cloud Task allows a user to develop and run short lived microservices using Spring Cloud and run them locally, in the cloud, even on Spring Cloud Data Flow. Just add @EnableTask and run your app as a Spring Boot app 

doc:
https://docs.spring.io/spring-cloud-task/docs/2.2.3.RELEASE/reference/#getting-started

examples:
https://www.baeldung.com/spring-cloud-task#1-adding-relevant-dependencies
https://github.com/spring-cloud/spring-cloud-task/tree/master/spring-cloud-task-samples

```bash
docker run -p 3306:3306 --name mysql -e MYSQL_ROOT_PASSWORD=passw0rd \
-e MYSQL_DATABASE=task -d mysql:5.7.25
```

## Reference
- [Stream Feature Guides](https://dataflow.spring.io/docs/feature-guides/streams/)
- [Spring Cloud Data FLow](https://dataflow.spring.io/getting-started/)
- [Stream Processing with Apache Kafka](https://dataflow.spring.io/docs/stream-developer-guides/streams/standalone-stream-kafka/)
- [Introduction to Spring Cloud Data Flow](https://sgitario.github.io/introduction-spring-data-flow/)
- [Getting Started with Spring Cloud Data Flow](https://www.e4developer.com/2018/02/18/getting-started-with-spring-cloud-data-flow/)
- [Spring for Apache Kafka Deep Dive](https://www.confluent.io/blog/spring-for-apache-kafka-deep-dive-part-3-apache-kafka-and-spring-cloud-data-flow/)
